webpackHotUpdate("static\\development\\pages\\about.js",{

/***/ "./components/About.js":
/*!*****************************!*\
  !*** ./components/About.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return About; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var react_scrollmagic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-scrollmagic */ "./node_modules/react-scrollmagic/dist/index.es.js");
/* harmony import */ var react_gsap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-gsap */ "./node_modules/react-gsap/dist/index.es.js");
var _jsxFileName = "C:\\Users\\Jens.Buyst\\projects\\jensbuyst\\frontend\\components\\About.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var AboutPage = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].div.withConfig({
  displayName: "About__AboutPage",
  componentId: "oaxm5x-0"
})(["width:100vw;height:100vh;position:relative;display:flex;align-items:center;flex-direction:column;section{position:absolute;}@media only screen and (max-width:1080px){background:url(\"./static/svg-about/bg-about-mobile-4.svg\") no-repeat bottom;}@media only screen and (min-width:1080px){background:url(\"./static/svg-about/bg-about-6.svg\") no-repeat bottom;background-size:cover;}"]);
var AboutImage1 = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].div.withConfig({
  displayName: "About__AboutImage1",
  componentId: "oaxm5x-1"
})(["width:150px;height:225px;border-radius:7px;background:url(\"./static/Jens.png\") no-repeat center;background-size:cover;margin:0 auto;"]);
var AboutImage2 = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].div.withConfig({
  displayName: "About__AboutImage2",
  componentId: "oaxm5x-2"
})(["width:150px;height:225px;border-radius:7px;background:url(\"./static/Jens-travel.png\") no-repeat center;background-size:cover;margin:0 auto;"]);
var AboutText = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].div.withConfig({
  displayName: "About__AboutText",
  componentId: "oaxm5x-3"
})(["width:50vw;position:absolute;left:15px;bottom:15px;line-height:1;letter-spacing:1.5px;font-size:1rem;text-align:justify;"]);

var About =
/*#__PURE__*/
function (_Component) {
  _inherits(About, _Component);

  function About() {
    _classCallCheck(this, About);

    return _possibleConstructorReturn(this, _getPrototypeOf(About).apply(this, arguments));
  }

  _createClass(About, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 60
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_scrollmagic__WEBPACK_IMPORTED_MODULE_2__["Controller"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 61
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_scrollmagic__WEBPACK_IMPORTED_MODULE_2__["Scene"], {
        triggerHook: "onLeave",
        duration: 3000,
        pin: true,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 62
        },
        __self: this
      }, function (progress) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(AboutPage, {
          __source: {
            fileName: _jsxFileName,
            lineNumber: 64
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_gsap__WEBPACK_IMPORTED_MODULE_3__["Timeline"], {
          totalProgress: progress,
          paused: true,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 65
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_gsap__WEBPACK_IMPORTED_MODULE_3__["Timeline"], {
          target: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 68
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
            className: "subtitle",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 69
            },
            __self: this
          }, "Who I am"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(AboutImage1, {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 70
            },
            __self: this
          })),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 66
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_gsap__WEBPACK_IMPORTED_MODULE_3__["Tween"], {
          to: {
            y: '0'
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 74
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_gsap__WEBPACK_IMPORTED_MODULE_3__["Timeline"], {
          target: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(AboutText, {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 77
            },
            __self: this
          }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 75
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_gsap__WEBPACK_IMPORTED_MODULE_3__["Tween"], {
          from: {
            y: '100',
            opacity: 0
          },
          to: {
            y: '0',
            opacity: 1
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 84
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_gsap__WEBPACK_IMPORTED_MODULE_3__["Tween"], {
          to: {
            y: '-300',
            opacity: 0
          },
          position: "+=2",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 85
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_gsap__WEBPACK_IMPORTED_MODULE_3__["Tween"], {
          to: {
            y: '-200',
            opacity: 0
          },
          position: "-=1",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 87
          },
          __self: this
        })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_gsap__WEBPACK_IMPORTED_MODULE_3__["Timeline"], {
          target: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 93
            },
            __self: this
          }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
            className: "subtitle",
            __source: {
              fileName: _jsxFileName,
              lineNumber: 94
            },
            __self: this
          }, "What I love"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(AboutImage2, {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 95
            },
            __self: this
          })),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 91
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_gsap__WEBPACK_IMPORTED_MODULE_3__["Tween"], {
          from: {
            y: '500',
            opacity: 0
          },
          to: {
            y: '0',
            opacity: 1
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 99
          },
          __self: this
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_gsap__WEBPACK_IMPORTED_MODULE_3__["Timeline"], {
          target: react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(AboutText, {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 102
            },
            __self: this
          }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."),
          __source: {
            fileName: _jsxFileName,
            lineNumber: 100
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_gsap__WEBPACK_IMPORTED_MODULE_3__["Tween"], {
          from: {
            y: '100',
            opacity: 0
          },
          to: {
            y: '0',
            opacity: 1
          },
          __source: {
            fileName: _jsxFileName,
            lineNumber: 109
          },
          __self: this
        })))));
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 121
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }, "next page ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }), " ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }))));
    }
  }]);

  return About;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);



/***/ })

})
//# sourceMappingURL=about.js.a6b5b1b2584f1648937d.hot-update.js.map