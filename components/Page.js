import React, { Component } from "react";
import Meta from "../components/Meta";
import Header from "./Header";
import styled, { ThemeProvider, injectGlobal } from "styled-components";

const theme = {
  red: "#FF0000",
  black: "#393939",
  grey: "#3A3A3A",
  lightgrey: "#E1E1E1"
};

// styled page
const StyledPage = styled.div`
  background: white;
  color: ${props => props.theme.black};
`;
// styled inner page
const Inner = styled.div`
  margin: 0 auto;
`;

injectGlobal`
  @font-face {
    font-family: 'SF';
    src: url('/static/SF-regular.otf');
    font-weight: normal;
    font-style: normal;
  }
  html {
    box-sizing: border-box;
    font-size: 15px;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  body {
    padding: 0;
    margin: 0;
    font-size: 1.5rem;
    line-height: 2;
    font-family: 'SF';
    overflow-y: auto !important;
  }
  a {
    text-decoration: none;
    color: ${theme.black};
  }
`;

export default class Page extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <StyledPage>
          <Meta />
          <Header />
          {this.props.children}
        </StyledPage>
      </ThemeProvider>
    );
  }
}
