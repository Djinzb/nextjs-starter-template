# NEXT JS STARTER PROJECT


* Bootstrap 4.1.3 (in <Meta />)
* Styled Components
* Apollo (in \_app.js + /utils/withData.js)
* nprogress included
* Lodash

>  Started from the [Advanced React and GrahpQL course](https://advancedreact.com/) I took from [Wes Bos](https://wesbos.com/). Removed all logic related to the course to have a starter project for future ones.

